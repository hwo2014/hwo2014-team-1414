package noobbot;
/*TEKIJ�: MARKO TOLVANEN
 * 
 * MUUTOKSET:
 * 24.4.2014: LIS�TTY AIKA-AJOT JA KISA KOODIIN.
 * 26.4.2014: LIS�TTY MAHDOLLISUUS TURBON K�YTT��N
 * 
 * */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }
    
    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        int kisanvaihe = 0;
        int omatesti = 0;
        String OmaAuto = "";
        String AutonRuutu = "";
        Integer iAutonRuutu = 0;
        Integer iAutonExRuutu = 0;
        Double dAutonRuutu = 0.0;
        String AutonSijainti = "";
        Integer iAutonSijainti = 0;
        Double dAutonSijainti = 0.0;
        String Rata[][] = new String[100][6];
        Double OmaThrottle = 0.0;
        Double KorjattuThrottle = 0.0;
        Double RadanPituus = 0.0;
        Double dKohta = 0.0;
        Double dAutonSlide = 0.0;
        boolean Turbo = false;
        
        send(join);
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                
            	
            	//send(new Throttle(0.6));
                
                //if (omatesti < 10) {
                AutonSijainnit mySijainti = gson.fromJson(line, AutonSijainnit.class);
                //System.out.println (mySijainti.getSijainti(OmaAuto));
                String[] testi = mySijainti.getSijainti(OmaAuto);
                //AutonRuutu = testi[0];
                //AutonSijainti = testi[1];
                dAutonRuutu = Double.parseDouble(testi[0]);
                iAutonRuutu = dAutonRuutu.intValue();
                if (iAutonRuutu > 0){
                  iAutonExRuutu = iAutonRuutu - 1;
                //}
                dAutonSijainti = Double.parseDouble(testi[1]);
                dAutonSlide = Double.parseDouble(testi[2]);
                //System.out.println ("Autoruutu(" + iAutonRuutu +") ja Sijainti(" + dAutonSijainti +")");
                }
                if ((dAutonSlide > 20) || (dAutonSlide < -20)) {
                	//System.out.println ("Autoruutu;" + iAutonRuutu +";Sijainti;" + dAutonSijainti +";Slide;" +dAutonSlide);
                }
                
                // T�SS� ON AI:N YDIN!!:)
                RadanPituus = Double.parseDouble(Rata[iAutonRuutu][2]);
                dKohta = dAutonSijainti / RadanPituus * 100;
        		if (dKohta < 20) {
        			OmaThrottle = Double.parseDouble(Rata[iAutonRuutu][3]);
        			if (((dAutonSlide > 23) || (dAutonSlide < -23)) && (OmaThrottle >= 0.3)) {
        				KorjattuThrottle = OmaThrottle - 0.1;
        				//System.out.println ("Oma Throttle:" + OmaThrottle +")");
        				if (iAutonExRuutu >=0){
        				  Rata[iAutonExRuutu][4] = Double.toString(KorjattuThrottle);
        				}
        			}
        			if (Rata[iAutonRuutu][5].equals("Turbo") && (Turbo = true )){
        				//send(new Turbo());
        				send(new Throttle(OmaThrottle));
        			}
        			send(new Throttle(OmaThrottle));	
        		}
        		else if (dKohta > 60) {
        			OmaThrottle = Double.parseDouble(Rata[iAutonRuutu][4]);
        			if (((dAutonSlide > 25) || (dAutonSlide < -25)) && (OmaThrottle >= 0.3)) {
        				KorjattuThrottle = OmaThrottle - 0.1;
        				//System.out.println ("Oma Throttle:" + OmaThrottle +")");
        				if (iAutonExRuutu >=0){
        				  Rata[iAutonRuutu][3] = Double.toString(KorjattuThrottle);
        				}
        			}
        			send(new Throttle(OmaThrottle));
        		}
        		else{
                //PISTET��N KAASUA!!
                  //send(new Throttle(0.6));
        		  send(new Throttle(OmaThrottle));
        		}
            
            } else {
            	if (msgFromServer.msgType.equals("join")) {
                   System.out.println("Joined");
            	} else if (msgFromServer.msgType.equals("turboAvailable")) {
                    //System.out.println("Kaytetaan turbo heti");
            		Turbo = true;
                    //send(new Turbo());
            	
            	} else if (msgFromServer.msgType.equals("lapFinished")) {
            		//System.out.println("Lap:" +msgFromServer.data.toString());
            	
            	} else if (msgFromServer.msgType.equals("crash")) {
                	//Joku ajoi kolarin ToDo
            	
            	} else if (msgFromServer.msgType.equals("spawn")) {
                	//Joku auto palasi kilpailuun crashin j�lkeen ToDo
            	
            	} else if (msgFromServer.msgType.equals("yourCar")) {
            		Car myCar = gson.fromJson(line, Car.class);
            		OmaAuto = myCar.getColor();
            		System.out.println("Tiimi:" + myCar.getName() +", Auto: " + OmaAuto);
            		
            		            
            	} else if (msgFromServer.msgType.equals("gameInit")&& kisanvaihe == 0) {
            		RataInit myRata = gson.fromJson(line, RataInit.class);
            		System.out.println("Radan nimi:" + myRata.getRadanNimi());
            		Rata = myRata.annaRataProfiili();
            		/*
            		for(int r=0; r < Rata.length; r++)
            		{
            		   for(int s=0; s < Rata[r].length; s++)
            		      System.out.print("\t" + Rata[r][s]);
            		   System.out.print("\n");
            		}*/
            		            			
           		        
            	} else if (msgFromServer.msgType.equals("gameInit")&& kisanvaihe == 2) {
            		System.out.println("Race init, Kisa");
            		kisanvaihe += 1;
            		
            	} else if (msgFromServer.msgType.equals("gameEnd")&& kisanvaihe == 1) {
            		kisanvaihe += 1;
            		System.out.println("Aika-ajot paattyy");
            		
            	} else if (msgFromServer.msgType.equals("gameStart") && kisanvaihe == 0) {
            		kisanvaihe += 1;
            		System.out.println("Aika-ajot alkaa");
            		
            	} else if (msgFromServer.msgType.equals("gameEnd")&& kisanvaihe == 4) {
            		System.out.println("Kisa paattyy");
            		
            	} else if (msgFromServer.msgType.equals("gameStart")&& kisanvaihe == 3) {
            		kisanvaihe += 1;
            		System.out.println("Kisa alkaa");
            		
            	} else if (msgFromServer.msgType.equals("tournamentEnd")) {
            		System.out.println("Turnaus paattyy");
            		
            	} else if (msgFromServer.msgType.equals("finish")) {
            		//System.out.println("Auto on maalissa");
            		
            	} else if (msgFromServer.msgType.equals("error")) {
            		
            		System.out.println("Error:" + msgFromServer.data.toString());
            		/*if (msgFromServer.data.toString().equals("Invalid throttle value"));
            		System.out.println("Invalid Throttle:(" +OmaThrottle +")");*/
            	} 
           		
            	else {
            	// jos tulee poikkeava messagetype:
            		System.out.println("Poikkeava message:" + msgFromServer.msgType.toString());
            	}
            	// l�hetet��n aina ping, jotta ei tiputa pelist�:)
                send(new Ping());   
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}
// 26.4.2014 MTO: tehty luokka oman testiradan k�ynnist�miseen.
class CreateRace extends SendMsg {
    public final String name;
    public final String key;

    CreateRace(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Turbo extends SendMsg { 
    
	@Override
    protected String msgType() {
        return "turbo";
	}    
    @Override
    protected Object msgData() {
            return "Turbo kehiin";
        }    
    }


class Car {
	private String msgType;
	private Data data;
   	private String gameId;
    
	   	public String getName() {
   	   		return data.name;
   	    }
	   	
   	   	public String getColor() {
   	   		return data.color;
   	    }
 	   	
  	   	
   	@Override
    public String toString() {
   		//return("Name: " + name + " (" + color + ")");
   		return("Message:" + msgType + "Data: " + data +  ") ja gameid:" +gameId );
   	}

class Data {
   		private String name;
   	   	private String color;
   	   	   	   	   	   	
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return("Name: " + name + " (" + color + ")");

   	   	}
}
}

class RataInit {
	private String msgType;
	private Data data;
   	    
	   	public String getRadanNimi() {
   	   		return data.race.track.name;
   	    }
   		   	
   	
	   	/*public  Pieces[] getPieces(){
	   		for(int i = 0; i < data.race.track.pieces.length; i++){
	   			if (data.race.track.pieces[i].length != null){
	   				System.out.println("RATA (" + i + ") Pituus:" +data.race.track.pieces[i].length);	
	   			}
	   			if (data.race.track.pieces[i].radius != null){
	   				System.out.println("RATA (" + i + ") S�de:" +data.race.track.pieces[i].radius + " ja kulma: " +data.race.track.pieces[i].angle) ;	
	   			}	   			
	   		}
	   		return data.race.track.pieces;
	   	}*/

	   	public  String[][] annaRataProfiili(){
	   		String Vastaus[][] = new String[100][6];
            double Piiri = 0.0;
            Integer Sade = 0;
            Double Kulma = 0.0;
            Double Pituus = 0.0;
            String MutkanSuunta = "";
            Integer Turbolaskuri = 0;
            Integer iTurbo = 0;   
            Integer iEdellinen = 0;
            Integer MaxRataKPL = 0;
            MaxRataKPL = data.race.track.pieces.length;
            MaxRataKPL = MaxRataKPL - 1;
            //System.out.println("MaxRataKPL:" +MaxRataKPL);
            
	   		for(int i = 0; i < data.race.track.pieces.length ; i++){
	   			if (data.race.track.pieces[i].length != null){
	   				Turbolaskuri += 1;
	   				Vastaus[i][0] = Integer.toString(i);
	   				Vastaus[i][1] = "suora";
	   				Vastaus[i][2] = data.race.track.pieces[i].length;
	   				Vastaus[i][3] = "0.8";
	   				Vastaus[i][4] = "0.8";
	   				Vastaus[i][5] = "";
	   				if (Turbolaskuri > 4){
	   					iTurbo = i - 4;
	   					if (iTurbo >= 0){
	   					  Vastaus[iTurbo][5] = "Turbo";
	   					}
	   				}
	   				//System.out.println("RATA (" + i + ") Pituus:" +data.race.track.pieces[i].length);	
	   			}
	   			if (data.race.track.pieces[i].radius != null){
	   				//System.out.println("RATA (" + i + ") S�de:" +data.race.track.pieces[i].radius + " ja kulma: " +data.race.track.pieces[i].angle) ;
	   				Kulma = Double.parseDouble(data.race.track.pieces[i].angle);
	   				Turbolaskuri = 0;
	   				if (Kulma < 0){
	   					MutkanSuunta = "Vasen";
	   					Kulma = Kulma * -1;
	   				}
	   				else {
	   					MutkanSuunta = "Oikea";
	   				}
	   				Sade = Integer.parseInt(data.race.track.pieces[i].radius);
	   				Piiri = 2 * 3.14 * Sade;
	   				Pituus = (Piiri / 360) * Kulma;
	   				Vastaus[i][0] = Integer.toString(i);
	   				Vastaus[i][1] = MutkanSuunta;
	   				Vastaus[i][2] = Double.toString(Pituus);
	   				Vastaus[i][3] = "0.6";
	   				Vastaus[i][4] = "0.6";
	   				Vastaus[i][5] = "";
	   				if (i >1){
	   					iEdellinen = i - 1;
	   					if (Vastaus[iEdellinen][1].equals("suora")){
	   						//hiljennetaan suoralla vauhtia
	   						Vastaus[iEdellinen][3] = "0.6";
	   						Vastaus[iEdellinen][4] = "0.4";
	   					}
	   				    else{
	   					// ToDo, jos jotain keksitaan.
	   					}
	   				}
	   			}
   				if (i == MaxRataKPL){
   					// ollaan viimeisell� radan p�tk�ll�. Tsekataan mik� on eka.
   					if ((Vastaus[i][1].equals("suora")) && (!"suora".equals(Vastaus[0][1]))){
   						//Jos viimeinen radan pala on suora ja ensimm�inen mutka, niin jarrutetaan.
   						Vastaus[i][3] = "0.7";
   						Vastaus[i][4] = "0.4";
   					}
   					//System.out.println("i:(" + i + ") ja MaxRataKPL:(" + MaxRataKPL + ")");
   					//System.out.println("Viimeinen mutka:" +Vastaus[i][1] + "ja ensimm�inen:" + Vastaus[0][1]);
   				}
	   		}
	   		return Vastaus;
	   	}
	   	
	   	
   	@Override
    public String toString() {
   		//return("Name: " + name + " (" + color + ")");
   		return("Message:" + msgType + "Data: " + data +  ")");
   	}
   	class Data {
   		private String name;
   	   	private String color;
   	   	private Race race;
   	   	   	   	
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return("Name: " + name + " (" + color + ")");

   	   	}
}
class Race {
		private Track track;
	   	   	   	
	   	@Override
	    public String toString() {
	   		//return("Name: " + name + " (" + color + ")");
	   		return("");

	   	}
}

class Track {
	private String id;
	private String name;
	public Pieces pieces[];
   	
   	@Override
    public String toString() {
   		//return("Name: " + name + " (" + color + ")");
   		return("Name: " + name + ")");

   	}
}

class Pieces{
	private String length;
	private String radius;
	private String angle;
	//private Boolean switch;
	
   	@Override
    public String toString() {
   		//return("Name: " + name + " (" + color + ")");
   		return "";
   	}
}
}

class AutonSijainnit {
	private String msgType;
	private Data data[];
   	    
   	public  String[] getSijainti(String Auto){
   		  String Vastaus[] = new String[3];

   		    //System.out.println ("Auto:" + Auto);
	   		for(int i = 0; i < data.length; i++){
	   			//if (data.race.track.pieces[i].length != null){
	   			if (Auto.equals(data[i].id.color)){
	   				//System.out.println("DATA (" + i + ") Pituus:" +data[i].id.color);
	   				//System.out.println("DATA (" + i + ") Pituus:" +data[i].piecePosition.pieceIndex);
	   				Vastaus[0] = data[i].piecePosition.pieceIndex;
	   				Vastaus[1] = data[i].piecePosition.inPieceDistance;
	   				Vastaus[2] = data[i].angle;
	   				//System.out.println("DATA (" + i + ") Pituus:" +data[i].piecePosition.inPieceDistance);
	   				i = data.length;
	   			}			
	   		}
	   		return Vastaus;
	   	}

	   	
   	@Override
    public String toString() {
   		//return("Name: " + name + " (" + color + ")");
   		return("Message:" + msgType + "Data: " + data +  ")");
   	}
   	
   	class Data {
   		private ID id;
   	   	private String angle;
   	   	private PiecePosition piecePosition;
   	   	private Lane lane;
   	   	
   	   	   	   	   	   	
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return "";

   	   	}
}
   	
   	class ID {
   		private String name;
   	   	private String color;
   	   	   	   	   	   	
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return("Name: " + name + " (" + color + ")");

   	   	}
}
   	class PiecePosition{
   		private String pieceIndex;
   		private String inPieceDistance;
   		private String lap;
   		  		
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return "";
   	   	}
   	}
   	
   	class Lane{
   		private String startLaneIndex;
   		private String endLaneIndex;
   		   		  		
   	   	@Override
   	    public String toString() {
   	   		//return("Name: " + name + " (" + color + ")");
   	   		return "";
   	   	}
   	}
}   	